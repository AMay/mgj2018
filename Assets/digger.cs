﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class digger : MonoBehaviour {

    public GameObject[] Diggame;
    public GameObject player;
    public GameObject button;
    public GameObject Buttons;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnDig()

    {


        GameObject[] hitColliders = GameObject.FindGameObjectsWithTag("dig");// Physics2D.OverlapCircleAll(player.transform.position, 0.5f);
        int i = 0;
        while (i < hitColliders.Length)
        {
            Debug.Log(Mathf.Abs((player.transform.position - hitColliders[i].transform.position).magnitude));
            if (Mathf.Abs((player.transform.position - hitColliders[i].transform.position).magnitude) < 1); 
            {
                OpenDig();
            }
        }
    }

    public void OpenDig()
    {
        int r = Random.Range(0, Diggame.Length);
        GameObject dig = Instantiate(Diggame[r], new Vector3(0,0,0), Quaternion.identity);
        dig.SetActive(true);
        dig.transform.parent = transform.parent.parent.parent;
        Buttons.SetActive(false);

    }

    void OnTriggerEnter2D()
    {
        button.SetActive(true);
    }
    void OnTriggerExit2D()
    {
        button.SetActive(false);
    }
}
