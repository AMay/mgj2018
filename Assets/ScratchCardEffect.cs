﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScratchCardEffect : MonoBehaviour {

    bool objectiveDone = false;
    private bool isTouching = false;
    public GameObject maskPrefab;
    private bool isPressed = false;
    public Camera mainCamera;
    public int clearTimer;
    public GameObject Buttons;
    Touch touch;
   void  Start()
    {
        
    }
	void OnEnable()
    {
        gameObject.transform.position = mainCamera.gameObject.transform.position + new Vector3(0,0,10);
    }
	// Update is called once per frame
	void Update () {
        
        //print(isPressed);
        var mousePos = Input.mousePosition;
        mousePos.z = 3;
        mousePos = mainCamera.ScreenToWorldPoint (mousePos);

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            isTouching = true;
        }
        else
            isTouching = false;

        if (isPressed == true)
        {
            GameObject maskSprite = Instantiate(maskPrefab, mousePos, Quaternion.identity);
            maskSprite.transform.parent = gameObject.transform;
        }
        else if (isTouching == true)
        {
            GameObject maskSprite = Instantiate(maskPrefab, touch.position, Quaternion.identity);
        }
        if (Input.GetMouseButtonDown(0))
            {
            Invoke("reveal", clearTimer);
                isPressed = true;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isPressed = false;
            }
        
	}


    void reveal()
    {
        objectiveDone = true;
        Buttons.SetActive(true);
        Destroy(gameObject);
    }
}
