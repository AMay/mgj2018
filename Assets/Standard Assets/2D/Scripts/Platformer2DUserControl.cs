using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        private bool m_Jump;
        public bool ap;
        public bool bb;
        public int i = 0;

        private int firstPressed = 0;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = 0; CrossPlatformInputManager.GetAxis("Horizontal");
            bool a = Input.GetKey(KeyCode.LeftArrow) || ap;
            bool b = Input.GetKey(KeyCode.RightArrow) || bb;

           /* if (firstPressed == 0)
            {
                if (a) firstPressed = -1;
                else if (b) firstPressed = 1;
            }

            if (a || b)
            {

            }*/

           
            if(a!=false||b!=false)
            {
                if(b)
                { h = 1; }
                if(a)
                { h = -1; }
                
            }
            else
            { i = 0; }
            if (Input.GetKey(KeyCode.UpArrow) && m_Jump != true)
            { m_Jump = true; }
            
            // Pass all parameters to the character control script.
            m_Character.Move(h, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
