﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;

public class Dialogue_Script : MonoBehaviour
{

    // Use this for initialization
    private SpriteRenderer spriteRenderer;
    int i = 0;
    public GameObject buttons;
    public GameObject tele;
    public GameObject cont;
    public GameObject textbut;
    bool started = false;
    bool touching = false;
    public Sprite[] testSpriteArray;
    
    void Start ()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
    void OnEnable()
    {
        started = true;
        cont.SetActive(false);
        textbut.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
      
    }

    public void CycleText()
    {
        if (i < 12)
        {
            i += 1;
            spriteRenderer.sprite = testSpriteArray[i];
        }
        else
        {

            cont.SetActive(true);
            tele.SetActive(true);
            buttons.SetActive(true);
            textbut.SetActive(false);
            gameObject.SetActive(false);
            
        }
    }
}
