﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleport : MonoBehaviour
{
    public Transform teleportLocation;
    private bool active = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(active)
        {
            active = false;
            GameObject go = other.gameObject;
            if (go.tag == "Player")
            {
                go.transform.position = teleportLocation.position;
            }
        }
        
    }
}
