﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets._2D;
public class setkey : MonoBehaviour {

    public Platformer2DUserControl p2d;
    public bool left;
    EventSystem es;
	// Use this for initialization
	void Start () {
        es = GameObject.Find("EventSystem").GetComponent<UnityEngine.EventSystems.EventSystem>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerUp ()
    {
        p2d.i = 0;
        if (!left)
        {
            
            p2d.bb = false;
        }
        else
        {
            p2d.ap = false;
        }
    }
    public void OnClick()
    {

        
        if (!left)
        {
            if (p2d.i == 0)
            {
                p2d.i = 1;
            }
            p2d.bb = true;
        }
        else
        {
            if (p2d.i == 0)
            {
                p2d.i = 2;
            }
            p2d.ap = true;
        }
    }

    void OnDisable()
    {
        p2d.ap = false;
        p2d.bb = false;
    }
}
